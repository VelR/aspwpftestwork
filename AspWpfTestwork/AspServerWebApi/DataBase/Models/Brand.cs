﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace AspServerWebApi.DataBase.Models
{
    /// <summary>
    /// Класс описывает сущность "Бренд".
    /// </summary>
    public class Brand : IModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// Название бренда.
        /// </summary>
        [BsonRequired] // Обязательный и уникальный.
        [BsonRepresentation(BsonType.String)]
        public string Name { get; set; }

        /// <summary>
        /// Краткая информация о бренде.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public string Description { get; set; }
    }
}
