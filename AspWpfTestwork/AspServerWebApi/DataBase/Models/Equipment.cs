﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AspServerWebApi.DataBase.Models
{
    /// <summary>
    /// Класс описывает сущность "Оборудование".
    /// </summary>
    public class Equipment : IModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// Название бренда.
        /// </summary>
        public Brand Brand { get; set; }

        /// <summary>
        /// Краткая информация о типе оборудования.
        /// </summary>
        public ToolType ToolType { get; set; }

        /// <summary>
        /// Краткие характеристики.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public decimal Price { get; set; }
    }
}
