﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AspServerWebApi.DataBase.Models
{
    /// <summary>
    /// Класс описывает сущность "Тип инструмента".
    /// </summary>
    public class ToolType: IModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// Название типа инструмента.
        /// </summary>
        [BsonRequired]  // Обязательный и уникальный.
        [BsonRepresentation(BsonType.String)]
        public string Name { get; set; }

        /// <summary>
        /// Описание вариантов прменения.
        /// </summary>
        [BsonRepresentation(BsonType.String)]
        public string DescriptionUseCase { get; set; }
    }
}
