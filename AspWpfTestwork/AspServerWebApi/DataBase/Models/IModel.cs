﻿namespace AspServerWebApi.DataBase.Models
{
    public interface IModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        string Id { get; set; }
    }
}
