﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AspServerWebApi.DataBase.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace AspServerWebApi.DataBase.CrudForModels
{
    /// <summary>
    /// Класс реализует стандартный CRUD функционал для текущей сущности.
    /// </summary>
    public class CrudMongoBase<TIModel> : ICrudAsync<TIModel>
    {
        /// <summary>
        /// Колекция для работы с сущностью.
        /// </summary>
        protected IMongoCollection<TIModel> Collection;
        
        /// <summary>
        /// Класс реализует стандартный CRUD функционал для текущей сущности.
        /// </summary>
        public CrudMongoBase(IMongoDatabase database)
        {
            Collection = database.GetCollection<TIModel>(typeof(TIModel).Name + "s");
        }

        /// <summary>
        /// Возвращает коллекцию текущей сущности, согласно фильтру.
        /// </summary>
        public virtual async Task<IEnumerable<TIModel>> Get(FilterDefinition<TIModel> filter)
        {
            return await Collection.Find(filter).ToListAsync();
        }

        /// <summary>
        /// Возвращает всю коллекцию текущей сущности.
        /// </summary>
        public virtual async Task<IEnumerable<TIModel>> Get()
        {
            return await (await Collection.FindAsync(_ => true)).ToListAsync();
        }

        /// <summary>
        /// Возвращает один элемент сущности по id.
        /// </summary>
        public virtual async Task<TIModel> GetById(string id)
        {
            return await Collection.Find(new BsonDocument("_id", new ObjectId(id))).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Добавление элемента сущности в БД.
        /// </summary>
        public virtual async Task Create(TIModel model)
        {
            await Collection.InsertOneAsync(model);
        }

        /// <summary>
        /// Обновление элемента сущности в БД.
        /// </summary>
        public virtual async Task Update(TIModel model)
        {
            await Collection.ReplaceOneAsync(new BsonDocument("_id", new ObjectId(((IModel)model).Id)), model);
        }

        /// <summary>
        /// Удаление элемента сущности из БД.
        /// </summary>
        public virtual async Task Remove(string id)
        {
            await Collection.DeleteOneAsync(new BsonDocument("_id", new ObjectId(id)));
        }
    }
}
