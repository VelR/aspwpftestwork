﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AspServerWebApi.DataBase.CrudForModels
{
    public interface ICrudAsync<TIModel>
    {
        /// <summary>
        /// Возвращает всю коллекцию текущей сущности.
        /// </summary>
        Task<IEnumerable<TIModel>> Get();

        /// <summary>
        /// Возвращает один элемент сущности по id.
        /// </summary>
        Task<TIModel> GetById(string id);

        /// <summary>
        /// Добавление элемента сущности в БД.
        /// </summary>
        Task Create(TIModel model);

        /// <summary>
        /// Обновление элемента сущности в БД.
        /// </summary>
        Task Update(TIModel model);

        /// <summary>
        /// Удаление элемента сущности из БД.
        /// </summary>
        Task Remove(string id);
    }
}
