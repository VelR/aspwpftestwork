﻿using AspServerWebApi.DataBase.CrudForModels;
using AspServerWebApi.DataBase.Models;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using MongoDB.Driver;

namespace AspServerWebApi.DataBase
{
    /// <summary>
    /// Класс описывает контекст для работы с БД.
    /// </summary>
    public class DbContext
    {
        #region Поля

        private CrudMongoBase<Brand> _brands;
        private CrudMongoBase<ToolType> _toolTypes;
        private CrudMongoBase<Equipment> _equipments;

        #endregion

        #region Свойства

        /// <summary>
        /// База данных MongoDb.
        /// </summary>
        protected IMongoDatabase Database;

        /// <summary>
        /// Доступ к CRUD колекции Brands.
        /// </summary>
        public ICrudAsync<Brand> Brands => _brands ?? (_brands = new CrudMongoBase<Brand>(Database));

        /// <summary>
        /// Доступ к CRUD колекции ToolTypes.
        /// </summary>
        public ICrudAsync<ToolType> ToolTypes => _toolTypes ?? (_toolTypes = new CrudMongoBase<ToolType>(Database));

        /// <summary>
        /// Доступ к CRUD колекции ToolTypes.
        /// </summary>
        public ICrudAsync<Equipment> Equipments => _equipments ?? (_equipments = new CrudMongoBase<Equipment>(Database));

        #endregion

        /// <summary>
        /// Создает контекст БД.
        /// </summary>
        public DbContext()
        {
            // Cтрока подключения.
            const string connectionString = "mongodb://localhost:27017/AspWpfTestwoorkDb";
            var connection = new MongoUrlBuilder(connectionString);

            // Клиент для взаимодействия с базой данных.
            var client = new MongoClient(connectionString);

            // Доступ к базе данных.
            Database = client.GetDatabase(connection.DatabaseName);
        }
    }
}
