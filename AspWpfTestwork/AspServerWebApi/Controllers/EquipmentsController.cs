﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using AspServerWebApi.DataBase.Models;
using Microsoft.AspNetCore.Mvc;

namespace AspServerWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipmentsController : Controller, ICrudController<Equipment>
    {
        /// <summary>
        /// GET api/TModels.
        /// </summary>
        /// <returns>Возвращает коллекцию TModel</returns>
        public ActionResult<IEnumerable<Equipment>> Get()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// GET api/TModels/id.
        /// </summary>
        /// <returns>Возвращает TModel</returns>
        public ActionResult<Equipment> Get(string id)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Post Method. Добавляет новый экземпляр TModel.
        /// </summary>
        public IActionResult Post(Equipment value)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// PUT Method. Изменяет элемент TModel.
        /// </summary>
        public IActionResult Put(Equipment value)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// DELETE Method. Удаляет заданный элемент TModel.
        /// </summary>
        public IActionResult Delete(Equipment value)
        {
            throw new System.NotImplementedException();
        }
    }
}
