﻿using System.Collections.Generic;
using AspServerWebApi.DataBase.CrudForModels;
using AspServerWebApi.DataBase.Models;
using Microsoft.AspNetCore.Mvc;

namespace AspServerWebApi.Controllers
{
    /// <summary>
    /// Класс описывает CRUD Controller для заданной модели.
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class CrudMongoDbBaseController<TModel> : Controller, ICrudController<TModel>
    {
        /// <summary>
        /// Доступ к CRUD колекции заданной модели.
        /// </summary>
        public ICrudAsync<TModel> DbCrudCollection { get; set; }

        /// <summary>
        /// GET api/TModels.
        /// </summary>
        /// <returns>Возвращает коллекцию TModel из БД</returns>
        [HttpGet]
        public virtual ActionResult<IEnumerable<TModel>> Get()
        {
            return new ActionResult<IEnumerable<TModel>>(DbCrudCollection.Get().Result);
        }

        /// <summary>
        /// GET api/TModels/id.
        /// </summary>
        /// <returns>Возвращает TModel из БД</returns>
        [HttpGet("{id}")]
        public virtual ActionResult<TModel> Get(string id)
        {
            return new ActionResult<TModel>(DbCrudCollection.GetById(id).Result);
        }

        /// <summary>
        /// Post Method. Добавляет новый экземпляр в бд.
        /// </summary>
        [HttpPost]
        public virtual IActionResult Post([FromBody] TModel value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            DbCrudCollection.Create(value);

            return Ok(value);
        }

        /// <summary>
        /// PUT Method. Изменяет элемент в бд.
        /// </summary>
        [HttpPut]
        public virtual IActionResult Put([FromBody] TModel value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            if (DbCrudCollection.GetById(((IModel)value).Id) == null)
            {
                return NotFound();
            }

            DbCrudCollection.Update(value);
            return Ok(value);
        }

        /// <summary>
        /// DELETE Method. Удаляет заданный экземпляр из БД
        /// </summary>
        [HttpDelete]
        public virtual IActionResult Delete([FromBody] TModel value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            if (DbCrudCollection.GetById(((IModel)value).Id) == null)
            {
                return NotFound();
            }
            DbCrudCollection.Remove(((IModel)value).Id);
            return Ok(value);
        }
    }
}
