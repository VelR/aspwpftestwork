﻿using AspServerWebApi.DataBase;
using AspServerWebApi.DataBase.Models;
using Microsoft.AspNetCore.Mvc;

namespace AspServerWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandsController : CrudMongoDbBaseController<Brand>
    {
        public BrandsController(DbContext dbContext)
        {
            DbCrudCollection = dbContext.Brands;
        }
    }
}
