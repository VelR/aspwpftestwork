﻿using AspServerWebApi.DataBase;
using AspServerWebApi.DataBase.Models;
using Microsoft.AspNetCore.Mvc;

namespace AspServerWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToolTypesController : CrudMongoDbBaseController<ToolType>
    {
        public ToolTypesController(DbContext dbContext)
        {
            DbCrudCollection = dbContext.ToolTypes;
        }
    }
}
