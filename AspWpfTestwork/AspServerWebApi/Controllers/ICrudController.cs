﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace AspServerWebApi.Controllers
{
    public interface ICrudController<TModel>
    {
        /// <summary>
        /// GET api/TModels.
        /// </summary>
        /// <returns>Возвращает коллекцию TModel</returns>
        [HttpGet]
        ActionResult<IEnumerable<TModel>> Get();

        /// <summary>
        /// GET api/TModels/id.
        /// </summary>
        /// <returns>Возвращает TModel</returns>
        [HttpGet("{id}")]
        ActionResult<TModel> Get(string id);

        /// <summary>
        /// Post Method. Добавляет новый экземпляр TModel.
        /// </summary>
        [HttpPost]
        IActionResult Post([FromBody] TModel value);

        /// <summary>
        /// PUT Method. Изменяет элемент TModel.
        /// </summary>
        [HttpPut]
        IActionResult Put([FromBody] TModel value);

        /// <summary>
        /// DELETE Method. Удаляет заданный элемент TModel.
        /// </summary>
        [HttpDelete]
        IActionResult Delete([FromBody] TModel value);
    }
}
