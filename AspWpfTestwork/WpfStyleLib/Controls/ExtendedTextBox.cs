﻿using System.Windows;
using System.Windows.Controls;

namespace WpfStyleLib.Controls
{
    /// <summary>
    /// Extended TextBox.
    /// </summary>
    class ExtendedTextBox : TextBox
    {
        /// <summary>
        /// Placeholder text property.
        /// </summary>
        public string PlaceHolderText
        {
            get { return (string)GetValue(PlaceHolderTextProperty); }
            set { SetValue(PlaceHolderTextProperty, value); }
        }

        public static readonly DependencyProperty PlaceHolderTextProperty =
            DependencyProperty.Register(nameof(PlaceHolderText), typeof(string), typeof(ExtendedTextBox), new PropertyMetadata(string.Empty));
    }
}
