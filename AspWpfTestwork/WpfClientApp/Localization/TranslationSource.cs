﻿using System.ComponentModel;
using System.Globalization;
using System.Resources;
using WpfClientApp.Localization.Resources;

namespace WpfClientApp.Localization
{
    public class TranslationSource : INotifyPropertyChanged
    {
        public static TranslationSource Instance { get; } = new TranslationSource();

        private readonly ResourceManager _resManager = Strings.ResourceManager;
        private CultureInfo _currentCulture;

        public string this[string key] => _resManager.GetString(key, _currentCulture);

        public CultureInfo CurrentCulture
        {
            get => _currentCulture;
            set
            {
                if (!Equals(_currentCulture, value))
                {
                    _currentCulture = value;
                    var @event = PropertyChanged;
                    @event?.Invoke(this, new PropertyChangedEventArgs(string.Empty));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
