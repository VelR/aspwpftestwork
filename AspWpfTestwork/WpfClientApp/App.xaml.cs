﻿using System;
using System.Globalization;
using System.Windows;
using WpfClientApp.DataManager;
using WpfClientApp.Localization;
using WpfClientApp.Mvvm.Models.Data;
using WpfClientApp.Mvvm.ViewModels;

namespace WpfClientApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static MainWindowViewModel MainWindowViewModel { get; set; }

        /// <summary>
        /// Обработка запуска приложения.
        /// </summary>
        private void ApplicationOnStartup(object sender, StartupEventArgs e)
        {
            try
            {
                StartApp();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// Запуск приложения.
        /// </summary>
        private void StartApp()
        {
            // Язык по умолчанию. ru-RU
            TranslationSource.Instance.CurrentCulture = new CultureInfo("ru-RU");

            DataManager.DataManager.Instance.Refresh();
            
            // Старт основного окна установки.
            MainWindowViewModel = new MainWindowViewModel();
            var mainWindow = new Mvvm.Views.MainWindowView() { DataContext = MainWindowViewModel };
            MainWindow = mainWindow;
            MainWindow.Show();
        }
    }
}
