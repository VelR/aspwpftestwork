﻿using System;
using System.Collections.Generic;
using RestSharp;

namespace WpfClientApp.DataManager
{
    /// <summary>
    /// API для работы с сервером.
    /// </summary>
    public class AspServerApi
    {
        /// <summary>
        /// URL сервера.
        /// </summary>
        const string BaseUrl = "https://localhost:5001/api";

        /// <summary>
        /// Выполнить Get запрос.
        /// </summary>
        public static T ExecuteGet<T>(RestRequest request) where T : new()
        {
            var client = new RestClient
            {
                BaseUrl = new Uri(BaseUrl)
            };

            var response = client.Execute<T>(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var exception = new ApplicationException(message, response.ErrorException);
                throw exception;
            }
            return response.Data;
        }

        /// <summary>
        /// Выполнить запрос. (Del, Create, Edit)
        /// </summary>
        public static ResponseStatus Execute<T>(RestRequest request) where T : new ()
        {
            var client = new RestClient
            {
                BaseUrl = new Uri(BaseUrl)
            };

            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<T>(request);
            return response.ResponseStatus;
        }

        /// <summary>
        /// Запрос на получение элементов базы данных.
        /// </summary>
        public static List<T> GetCollection<T>() where T : new()
        {
            var request = new RestRequest
            {
                Resource = typeof(T).Name + "s"
            };
            //request.RootElement = "Call";
            
            return ExecuteGet<List<T>>(request);
        }

        /// <summary>
        /// Добавить новый элемент в БД.
        /// </summary>
        public static void Create<T>(T model) where T : new()
        {
            var request = new RestRequest
            {
                Resource = typeof(T).Name + "s",
                Method = Method.POST,
            };

            request.AddJsonBody(model);

            Execute<T>(request);
        }

        /// <summary>
        /// Редактировать элемент в БД.
        /// </summary>
        public static void Edit<T>(T model) where T : new()
        {
            var request = new RestRequest
            {
                Resource = typeof(T).Name + "s",
                Method = Method.PUT,
            };

            request.AddJsonBody(model);

            Execute<T>(request);
        }

        /// <summary>
        /// Редактировать элемент в БД.
        /// </summary>
        public static void Delete<T>(T model) where T : new()
        {
            var request = new RestRequest
            {
                Resource = typeof(T).Name + "s",
                Method = Method.DELETE,
            };

            request.AddJsonBody(model);

            Execute<T>(request);
        }
    }
}
