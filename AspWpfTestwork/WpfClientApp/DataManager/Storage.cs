﻿using System.Collections.ObjectModel;
using System.Windows;
using WpfClientApp.Mvvm.Models;
using WpfClientApp.Mvvm.Models.Data;

namespace WpfClientApp.DataManager
{
    public class Storage: DependencyObject
    {
        public static Storage Instance { get; } = new Storage();

        public ObservableCollection<Brand> Brands
        {
            get => (ObservableCollection<Brand>)GetValue(BrandsProperty);
            set => SetValue(BrandsProperty, value);
        }

        public static readonly DependencyProperty BrandsProperty =
            DependencyProperty.Register(nameof(Brands), typeof(ObservableCollection<Brand>), typeof(Storage), new PropertyMetadata(new ObservableCollection<Brand>()));

        
        public ObservableCollection<ToolType> ToolTypes
        {
            get => (ObservableCollection<ToolType>)GetValue(ToolTypesProperty);
            set => SetValue(ToolTypesProperty, value);
        }

        public static readonly DependencyProperty ToolTypesProperty =
            DependencyProperty.Register(nameof(ToolTypes), typeof(ObservableCollection<ToolType>), typeof(Storage), new PropertyMetadata(new ObservableCollection<ToolType>()));


        public ObservableCollection<Equipment> Equipments
        {
            get => (ObservableCollection<Equipment>)GetValue(EquipmentsProperty);
            set => SetValue(EquipmentsProperty, value);
        }

        public static readonly DependencyProperty EquipmentsProperty =
            DependencyProperty.Register(nameof(Equipments), typeof(ObservableCollection<Equipment>), typeof(Storage), new PropertyMetadata(new ObservableCollection<Equipment>()));
    }
}
