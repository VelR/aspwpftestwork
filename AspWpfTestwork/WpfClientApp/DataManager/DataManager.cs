﻿using System.Collections.ObjectModel;
using WpfClientApp.Mvvm.Models;
using WpfClientApp.Mvvm.Models.Data;

namespace WpfClientApp.DataManager
{
    public class DataManager
    {
        public Storage Storage => Storage.Instance;

        public static DataManager Instance { get; } = new DataManager();

        public void Refresh()
        {
            Storage.Brands = new ObservableCollection<Brand>(AspServerApi.GetCollection<Brand>());
            Storage.ToolTypes = new ObservableCollection<ToolType>(AspServerApi.GetCollection<ToolType>());
            Storage.Equipments = new ObservableCollection<Equipment>(AspServerApi.GetCollection<Equipment>());
        }
    }
}
