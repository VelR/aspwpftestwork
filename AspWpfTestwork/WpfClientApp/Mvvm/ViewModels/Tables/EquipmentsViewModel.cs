﻿using System.Collections.ObjectModel;
using System.Windows;
using MaterialDesignThemes.Wpf;
using WpfClientApp.DataManager;
using WpfClientApp.Localization;
using WpfClientApp.Localization.Resources;
using WpfClientApp.Mvvm.Models;
using WpfClientApp.Mvvm.ViewModels.Dialogs;
using WpfClientApp.Mvvm.Views.Dialogs;

namespace WpfClientApp.Mvvm.ViewModels.Tables
{
    public class EquipmentsViewModel : BaseTableViewModel<Equipment>
    {
        #region Свойства

        /// <summary>
        /// Колекция из бд отображаемая в таблице.
        /// </summary>
        public override ObservableCollection<Equipment> ModelCollection => Storage.Instance.Equipments;

        /// <summary>
        /// Наименование таблицы.
        /// </summary>
        public override string Name
        {
            get => (string)GetValue(NameProperty);
            set => SetValue(NameProperty, value);
        }

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register(nameof(Name), typeof(string), typeof(EquipmentsViewModel), new PropertyMetadata(Strings.Tables_Equipment));
        
        #endregion

        #region Конструктор

        public EquipmentsViewModel()
        {
            TranslationSource.Instance.PropertyChanged += (sender, args) =>
            {
                Name = TranslationSource.Instance[nameof(Strings.Tables_Equipment)];
            };
        }

        #endregion

        #region Методы

        /// <summary>
        /// Добавить новый элемент в бд.
        /// </summary>
        protected override async void Add(object obj)
        {
            var view = new CreateEditDialog()
            {
                DataContext = new EquipmentViewModel()
            };

            await DialogHost.Show(view, "RootDialog", CreateClosingEventHandler);
        }

        /// <summary>
        /// Редактировать элемент в бд.
        /// </summary>
        protected override async void Edit(object obj)
        {
            var toolType = (Equipment)obj;

            var view = new CreateEditDialog()
            {
                DataContext = new EquipmentViewModel(toolType)
            };

            await DialogHost.Show(view, "RootDialog", CreateClosingEventHandler);
        }

        #endregion
    }
}
