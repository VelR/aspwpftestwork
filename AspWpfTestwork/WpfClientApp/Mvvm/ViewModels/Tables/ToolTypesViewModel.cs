﻿using System.Collections.ObjectModel;
using System.Windows;
using MaterialDesignThemes.Wpf;
using WpfClientApp.DataManager;
using WpfClientApp.Localization;
using WpfClientApp.Localization.Resources;
using WpfClientApp.Mvvm.Models;
using WpfClientApp.Mvvm.ViewModels.Dialogs;
using WpfClientApp.Mvvm.Views.Dialogs;

namespace WpfClientApp.Mvvm.ViewModels.Tables
{
    public class ToolTypesViewModel : BaseTableViewModel<ToolType>
    {
        #region Свойства

        /// <summary>
        /// Элементы таблицы БД.
        /// </summary>
        public override ObservableCollection<ToolType> ModelCollection => Storage.Instance.ToolTypes;

        /// <summary>
        /// Наименование таблицы.
        /// </summary>
        public override string Name
        {
            get => (string)GetValue(NameProperty);
            set => SetValue(NameProperty, value);
        }

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register(nameof(Name), typeof(string), typeof(ToolTypesViewModel), new PropertyMetadata(Strings.Tables_ToolType));

        #endregion

        #region Конструктор

        /// <summary>
        /// Создает VM таба "Типы оборудования".
        /// </summary>
        public ToolTypesViewModel()
        {
            TranslationSource.Instance.PropertyChanged += (sender, args) =>
            {
                Name = TranslationSource.Instance[nameof(Strings.Tables_ToolType)];
            };
        }

        #endregion

        #region Методы

        /// <summary>
        /// Добавить новый элемент в бд.
        /// </summary>
        protected override async void Add(object obj)
        {
            var view = new CreateEditDialog()
            {
                DataContext = new ToolTypeViewModel()
            };

            await DialogHost.Show(view, "RootDialog", CreateClosingEventHandler);
        }

        /// <summary>
        /// Редактировать элемент в бд.
        /// </summary>
        protected override async void Edit(object obj)
        {
            var toolType = (ToolType)obj;

            var view = new CreateEditDialog()
            {
                DataContext = new ToolTypeViewModel(toolType)
            };

            await DialogHost.Show(view, "RootDialog", CreateClosingEventHandler);
        }

        #endregion
    }
}
