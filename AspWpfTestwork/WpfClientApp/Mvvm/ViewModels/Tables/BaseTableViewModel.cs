﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using WpfClientApp.DataManager;
using WpfStyleLib.Common;

namespace WpfClientApp.Mvvm.ViewModels.Tables
{
    public abstract class BaseTableViewModel<TModel>: DependencyObject, ITableViewModel where TModel : new()
    {
        /// <summary>
        /// Наименование таблицы.
        /// </summary>
        public abstract string Name { get; set; }

        /// <summary>
        /// Колекция из бд отображаемая в таблице.
        /// </summary>
        public abstract ObservableCollection<TModel> ModelCollection { get; }
        
        /// <summary>
        /// Комманда добавления нового бренда в бд.
        /// </summary>
        public ICommand AddCommand { get; set; }

        /// <summary>
        /// Комманда редактирования бренда.
        /// </summary>
        public ICommand EditCommand { get; set; }

        /// <summary>
        /// Комманда удаления существующего бренд из бд.
        /// </summary>
        public ICommand RemoveCommand { get; set; }

        protected BaseTableViewModel()
        {
            AddCommand = new RelayCommand(Add);
            EditCommand = new RelayCommand(Edit);
            RemoveCommand = new RelayCommand(Remove);
        }

        /// <summary>
        /// Обработчик закрытия окна "Редактирования бренда".
        /// </summary>
        protected void EditClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter != null)
            {
                var element = (TModel)eventArgs.Parameter;
                AspServerApi.Edit(element);
            }
        }

        /// <summary>
        /// Обработчик закрытия окна "Редактирования бренда".
        /// </summary>
        protected void CreateClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter != null)
            {
                var element = (TModel)eventArgs.Parameter;
                AspServerApi.Create(element);
            }
        }

        /// <summary>
        /// Добавить новый элемент в бд.
        /// </summary>
        protected abstract void Add(object obj);

        /// <summary>
        /// Редактировать элемент в бд.
        /// </summary>
        protected abstract void Edit(object obj);

        /// <summary>
        /// Удаление бренда из бд.
        /// </summary>
        public void Remove(object obj)
        {
            var element = (TModel)obj;
            AspServerApi.Delete(element);
        }
    }
}
