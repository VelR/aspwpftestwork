﻿using System.Windows.Input;

namespace WpfClientApp.Mvvm.ViewModels.Tables
{
    /// <summary>
    /// Описывает интерфейс модели представления для таблиц.
    /// </summary>
    public interface ITableViewModel
    {
        /// <summary>
        /// Наименование таблицы.
        /// </summary>
        string Name { get; }
        
        /// <summary>
        /// Комманда добавления нового элемента в бд.
        /// </summary>
        ICommand AddCommand { get; set; }

        /// <summary>
        /// Комманда редактирования элемента.
        /// </summary>
        ICommand EditCommand { get; set; }

        /// <summary>
        /// Комманда удаления существующего элемента из бд.
        /// </summary>
        ICommand RemoveCommand { get; set; }
    }
}
