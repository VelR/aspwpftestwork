﻿using System.Collections.ObjectModel;
using System.Windows;
using MaterialDesignThemes.Wpf;
using WpfClientApp.DataManager;
using WpfClientApp.Localization;
using WpfClientApp.Localization.Resources;
using WpfClientApp.Mvvm.Models.Data;
using WpfClientApp.Mvvm.ViewModels.Dialogs;
using WpfClientApp.Mvvm.Views.Dialogs;

namespace WpfClientApp.Mvvm.ViewModels.Tables
{
    /// <summary>
    /// VM таба бренды.
    /// </summary>
    public class BrandsViewModel : BaseTableViewModel<Brand>
    {
        #region Свойства

        /// <summary>
        /// Колекция из бд отображаемая в таблице.
        /// </summary>
        public override ObservableCollection<Brand> ModelCollection => Storage.Instance.Brands;

        /// <summary>
        /// Наименование таблицы.
        /// </summary>
        public override string Name
        {
            get => (string)GetValue(NameProperty);
            set => SetValue(NameProperty, value);
        }

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register(nameof(Name), typeof(string), typeof(BrandsViewModel), new PropertyMetadata(Strings.Tables_Brand));

        #endregion

        #region Конструктор

        /// <summary>
        /// Создает VM таба бренды.
        /// </summary>
        public BrandsViewModel()
        {
            TranslationSource.Instance.PropertyChanged += (sender, args) =>
            {
                Name = TranslationSource.Instance[nameof(Strings.Tables_Brand)];
            };
        }

        #endregion

        #region Команды

        /// <summary>
        /// Добавить новый бренд в бд.
        /// </summary>
        protected override async void Add(object obj)
        {
            var view = new CreateEditDialog()
            {
                DataContext = new BrandViewModel()
            };

            await DialogHost.Show(view, "RootDialog", CreateClosingEventHandler);
        }

        /// <summary>
        /// Редактировать бренд в бд.
        /// </summary>
        protected override async void Edit(object obj)
        {
            var brand = (Brand)obj;

            var view = new CreateEditDialog()
            {
                DataContext = new BrandViewModel(brand)
            };

            await DialogHost.Show(view, "RootDialog", EditClosingEventHandler);
        }
        
        #endregion
    }
}
