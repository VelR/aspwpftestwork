﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using WpfClientApp.Localization;
using WpfClientApp.Mvvm.ViewModels.Tables;

namespace WpfClientApp.Mvvm.ViewModels
{
    /// <summary>
    /// ViewModel основного окна приложения.
    /// </summary>
    public class MainWindowViewModel: DependencyObject
    {
        public static IList<ITableViewModel> ViewModels => new List<ITableViewModel>
        {
            new BrandsViewModel(),
            new ToolTypesViewModel(),
            new EquipmentsViewModel()
        };

        public object SelectedViewModel
        {
            get => (ITableViewModel)GetValue(SelectedViewModelProperty);
            set => SetValue(SelectedViewModelProperty, value);
        }

        public static readonly DependencyProperty SelectedViewModelProperty =
            DependencyProperty.Register(nameof(SelectedViewModel), typeof(ITableViewModel), typeof(MainWindowViewModel), new PropertyMetadata(null));


        /// <summary>
        /// Применяемые языки.
        /// </summary>
        private static readonly IReadOnlyCollection<CultureInfo> Languages = new Collection<CultureInfo>()
        {
            new CultureInfo("ru-RU"),
            new CultureInfo("en-US"),
        };

        /// <summary>
        /// Языки для установки.
        /// </summary>
        public IReadOnlyCollection<CultureInfo> LanguagesSourse => Languages;
        
        /// <summary>
        /// Выбранный язык.
        /// </summary>
        public CultureInfo SelectedLanguage
        {
            get => (CultureInfo)GetValue(SelectedLanguageProperty);
            set => SetValue(SelectedLanguageProperty, value);
        }

        public static readonly DependencyProperty SelectedLanguageProperty =
            DependencyProperty.Register(nameof(SelectedLanguage), typeof(CultureInfo), typeof(MainWindowViewModel), new PropertyMetadata(Languages.First(x => Equals(x, TranslationSource.Instance.CurrentCulture)),
                (o, args) =>
                {
                    TranslationSource.Instance.CurrentCulture = (CultureInfo)args.NewValue;
                }));
    }
}
