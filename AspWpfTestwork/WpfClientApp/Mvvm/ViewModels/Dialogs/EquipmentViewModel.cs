﻿using WpfClientApp.Mvvm.Models;

namespace WpfClientApp.Mvvm.ViewModels.Dialogs
{
    /// <summary>
    /// Отображение информации о типе оборудования в диалоге.
    /// </summary>
    public class EquipmentViewModel : CreateEditBaseViewModel<Equipment>
    {
        #region Конструктор

        public EquipmentViewModel(Equipment model = null) : base(model)
        {
        }

        #endregion

        #region Методы

        /// <summary>
        /// Валидация данных.
        /// </summary>
        public override bool Validate()
        {
            var isValid = true;

            if (Model.Brand == null)
            {
                isValid = false;
            }
            else if (Model.ToolType == null)
            {
                isValid = false;
            }
            else if (string.IsNullOrWhiteSpace(Model.Description))
            {
                isValid = false;
            }

            return isValid;
        }

        #endregion
    }
}
