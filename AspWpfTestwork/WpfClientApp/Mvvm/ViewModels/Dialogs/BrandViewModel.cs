﻿using WpfClientApp.Mvvm.Models.Data;

namespace WpfClientApp.Mvvm.ViewModels.Dialogs
{
    /// <summary>
    /// Отображение информации о бренде в диалоге.
    /// </summary>
    public class BrandViewModel: CreateEditBaseViewModel<Brand>
    {
        #region Конструктор

        public BrandViewModel(Brand model = null) : base(model)
        {
        }

        #endregion

        #region Методы

        /// <summary>
        /// Валидация данных.
        /// </summary>
        public override bool Validate()
        {
            var isValid = true;

            if (string.IsNullOrWhiteSpace(Model.Name))
            {
                isValid = false;
            }
            else if (string.IsNullOrWhiteSpace(Model.Description))
            {
                isValid = false;
            }

            return isValid;
        }

        #endregion
    }
}
