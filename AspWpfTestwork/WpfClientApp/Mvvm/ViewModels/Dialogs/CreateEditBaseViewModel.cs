﻿using System.Windows;
using MaterialDesignThemes.Wpf;
using WpfClientApp.DataManager;
using WpfClientApp.Localization;
using WpfClientApp.Localization.Resources;
using WpfStyleLib.Common;

namespace WpfClientApp.Mvvm.ViewModels.Dialogs
{
    /// <summary>
    /// VM диалогового окна для создания или редактирования элемента модели.
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class CreateEditBaseViewModel<TModel>: DependencyObject where TModel : new()
    {
        #region Свойства

        /// <summary>
        /// Диалог в режиме редактирования.
        /// </summary>
        public bool IsEdit { get; set; }

        /// <summary>
        /// Валидация данных.
        /// </summary>
        public abstract bool Validate();

        /// <summary>
        /// Загаловок диалога.
        /// </summary>
        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register(nameof(Title), typeof(string), typeof(CreateEditBaseViewModel<TModel>), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Текст на кнопке "Создать/Сохранить".
        /// </summary>
        public string AcceptButtonText
        {
            get => (string)GetValue(AcceptButtonTextProperty);
            set => SetValue(AcceptButtonTextProperty, value);
        }

        public static readonly DependencyProperty AcceptButtonTextProperty =
            DependencyProperty.Register(nameof(AcceptButtonText), typeof(string), typeof(CreateEditBaseViewModel<TModel>), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Бренд.
        /// </summary>
        public TModel Model
        {
            get => (TModel)GetValue(ModelProperty);
            set => SetValue(ModelProperty, value);
        }

        public static readonly DependencyProperty ModelProperty =
            DependencyProperty.Register(nameof(Model), typeof(TModel), typeof(CreateEditBaseViewModel<TModel>), new PropertyMetadata(null));

        /// <summary>
        /// Доступ к Storage.
        /// </summary>
        public Storage Storage => Storage.Instance;

        #endregion


        protected CreateEditBaseViewModel(TModel model)
        {
            IsEdit = model != null;
            Model = IsEdit ? model : new TModel();
            AcceptButtonText = IsEdit ? TranslationSource.Instance[nameof(Strings.Dialog_Button_Save)] : TranslationSource.Instance[nameof(Strings.Dialog_Button_Create)];
            Title = IsEdit ? TranslationSource.Instance[nameof(Strings.Dialog_Title_Edit)] : TranslationSource.Instance[nameof(Strings.Dialog_Title_Create)];
        }

        /// <summary>
        /// Команда сохранения элемента или добавления.
        /// </summary>
        public RelayCommand AcceptCommand =>
            new RelayCommand(obj =>
            {
                var parametr = Model;
                DialogHost.CloseDialogCommand.Execute(parametr, null);
            }, o => Validate());
    }
}
