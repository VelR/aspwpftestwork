﻿using WpfClientApp.Mvvm.Models;

namespace WpfClientApp.Mvvm.ViewModels.Dialogs
{
    /// <summary>
    /// Отображение информации о типе оборудования в диалоге.
    /// </summary>
    public class ToolTypeViewModel : CreateEditBaseViewModel<ToolType>
    {
        #region Конструктор

        public ToolTypeViewModel(ToolType model = null) : base(model)
        {
        }

        #endregion

        #region Методы

        /// <summary>
        /// Валидация данных.
        /// </summary>
        public override bool Validate()
        {
            var isValid = true;

            if (string.IsNullOrWhiteSpace(Model.Name))
            {
                isValid = false;
            }
            else if (string.IsNullOrWhiteSpace(Model.DescriptionUseCase))
            {
                isValid = false;
            }

            return isValid;
        }

        #endregion
    }
}
