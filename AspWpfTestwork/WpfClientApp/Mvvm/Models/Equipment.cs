﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using WpfClientApp.Annotations;
using WpfClientApp.Mvvm.Models.Data;

namespace WpfClientApp.Mvvm.Models
{
    /// <summary>
    /// Класс описывает модель "Оборудование".
    /// </summary>
    public class Equipment: INotifyPropertyChanged
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Название бренда.
        /// </summary>
        public Brand Brand { get; set; }

        /// <summary>
        /// Краткая информация о типе оборудования.
        /// </summary>
        public ToolType ToolType { get; set; }

        /// <summary>
        /// Краткие характеристики.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public decimal Price { get; set; }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
