﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using WpfClientApp.Annotations;

namespace WpfClientApp.Mvvm.Models
{
    /// <summary>
    /// Класс описывает модель "Тип инструмента".
    /// </summary>
    public class ToolType: INotifyPropertyChanged
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Название типа инструмента.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание вариантов прменения.
        /// </summary>
        public string DescriptionUseCase { get; set; }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
