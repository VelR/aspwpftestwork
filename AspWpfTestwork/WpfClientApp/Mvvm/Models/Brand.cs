﻿using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WpfClientApp.Annotations;

namespace WpfClientApp.Mvvm.Models.Data
{
    /// <summary>
    /// Класс описывает модель "Бренд".
    /// </summary>
    public class Brand: INotifyPropertyChanged
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Название бренда.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Краткая информация о бренде.
        /// </summary>
        public string Description { get; set; }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
