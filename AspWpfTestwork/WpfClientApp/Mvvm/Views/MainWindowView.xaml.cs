﻿using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls;

namespace WpfClientApp.Mvvm.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : MetroWindow
    {
        public MainWindowView()
        {
            InitializeComponent();
        }
    }
}
